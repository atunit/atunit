{-# LANGUAGE FlexibleInstances
, OverloadedStrings
, TemplateHaskell #-}

module Main where

import           Snap
import           Snap.Snaplet.Heist
import           Snap.Snaplet.PostgresqlSimple
import qualified Data.ByteString.Char8 as B
import           Control.Lens hiding (index)
import           Site

-- Create the App object

data App = App
           { _heist :: Snaplet (Heist App)
           , _pg :: Snaplet Postgres
           }

makeLenses ''App

instance HasHeist App where
  heistLens = subSnaplet heist

instance HasPostgres (Handler b App) where
  getPostgresState = with pg get

-- Build a routing table that describes how URLs are handled

routes :: [(B.ByteString, Handler App App ())]
routes = [ ("/", index)
         , ("/project/new", method POST createNewProject)
         , ("/projects", method GET getAllProjects)
         , ("/project", method DELETE deleteProjectByTitle)
         ]

app :: SnapletInit App App
app = makeSnaplet "app" "My stunningly advanced Snap application." Nothing $ do
  h <- nestSnaplet "heist" heist $ heistInit "templates"
  p <- nestSnaplet "pg" pg pgsInit
  addRoutes routes
  return $ App h p

index :: Handler App App ()
index = render "index"

createNewProject :: Handler App App ()
createNewProject = do
  ti <- getPostParam "title"
  de <- getPostParam "description"
  _ <- execute "INSERT INTO projects VALUES (?, ?)" (ti, de)
  redirect "/"

getAllProjects :: Handler App App ()
getAllProjects = do
  allProjects <- query_ "SELECT * FROM projects"
  liftIO $ print (allProjects :: [Project])

deleteProjectByTitle :: Handler App App ()
deleteProjectByTitle = do
  ti <- getPostParam "title"
  _ <- execute "DELETE FROM projects WHERE title = ?" (Only ti)
  redirect "/"

-- Startup the server with this configuration

main :: IO ()
main = do serveSnaplet defaultConfig app
