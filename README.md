# Welcome to the ATUnit Project

## About:

We are building a welcoming and sustainable funding community. This platform is largely a response to the [Gittip crisis](http://geekfeminism.wikia.com/wiki/Gittip_crisis).

## What to do?

You can come talk to us at [#atunit on freenode](http://webchat.freenode.net/?channels=%23atunit), which is basically a chatroom. Introduce yourself and ask what's being worked on!

If you already know a bit about projects like this, you can go to the [issues list](https://www.assembla.com/spaces/atunit/tickets/). The issues list is basically a discussion board of what needs to happen. You can watch the project to get an email when new issues are created. One will probably pop up that you can help with!

**Below is a list of broad project areas, what's happening with them, what help is needed.**

### Design

* **Currently:** answering broad UX questions (i.e. who are our users? what do they want do to?)
* **Next up:** construct a UX survey!
* **Help needed:** website UI / UX people. People from the target audience. People familiar with Gittip to some degree.

### Tech

* **Recently finished:** decided the web framework would be in haskell!
* **Next up:** {{WIP}}
* **Help needed:** Haskell web developer

### Organization

* **Currently:** figuring out bylaws, deciding on a specific organizational structure (preferably a cooperative!)
* **Help needed:** A lawyer, people familiar with cooperatives.

## Useful links:

- [Website](https://www.atunit.org/) (not much there right now)
- [#atunit](http://webchat.freenode.net/?channels=%23atunit) on freenode (chat room)
- [Issues and discussion](https://www.assembla.com/spaces/atunit/tickets/)

## [F]requently [A]sked [Q]uestions:

* **Q: Is it ready yet?**
    * It is not! There is currently no plan for a specific release date. But we are currently working on hashing out what our release features would need to be so we can get there as soon as is reasonable!
* **Q: Can I help?**
    * Yes! We need lots more people to help out!
* **Q: I'm not a programmer, can I still help out?**
    * Also yes! This is a very big project and needs lots of different sorts of people! [The issues list](https://gitlab.com/atunit/atunit/issues) should give a good overview of the types of things that need to happen
* **How do I get updates on what's happening with the project?**
    * (instructions to watch the repo)
* **Q: Why not [some other service]?**
    * Paypal: {{WIP}}
    * Patreon: {{WIP}}
* **How do I get updates on what's happening with the project?**
    * {{WIP}}
* **What projects are there related to this?**
    * [Healing Rain](http://healingra.in/)
    * \#NotHaskell
